import express from 'express';
import multer from 'multer';
import bodyParser from 'body-parser';
import { Client } from 'pg';

let port = 8100;
let app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

let fileCounter = 0;
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads');
  },
  filename: (req, file, cb) => {
    // console.log('filename of file:', file);
    let ext = file.mimetype.split('/').pop();
    fileCounter++;
    let filename = `${Date.now()}-${fileCounter}.${ext}`;
    cb(null, filename);
  },
});
let upload = multer({ storage });

let client = new Client({
  database: 'postgres',
  user: 'postgres',
  password: 'mysecretpassword',
});

async function connect() {
  try {
    await client.connect();
    client.query(`
    create table if not exists trip (
        id serial primary key,
        user_id text
    );
    create table if not exists point (
        id serial primary key,
        trip_id integer,
        num integer,
        location text,
        memo text,
        foreign key (trip_id) references trip(id)
    );
    create table if not exists file (
        id serial primary key,
        point_id integer,
        filename text,
        foreign key (point_id) references point(id)
    );
    `);
  } catch (e) {
    console.log('failed to connect database:', e);
  }
}
connect();

async function dropTables() {
  await client.query(`
    drop table if exists file;
    drop table if exists point;
    drop table if exists trip;
    `);
}
// dropTables();

app.post('/trip', async (req, res) => {
  try {
    let json = await savePoints(req.body.points);
    res.json(json);
  } catch (e) {
    res.status(500).json({ error: e.toString() });
  }
});
app.post('/point/files', upload.array('files'), async (req, res) => {
  try {
    console.log('save files');
    // console.log('body:', req.body);
    // console.log('files:', req.files);
    let { point_id } = req.body;
    let files = [];
    for (let i = 0; i < req.files.length; i++) {
      let file = req.files[i];
      let { filename } = file;
      let file_id = await saveFile(point_id, filename);
      files.push({ file_id, filename });
    }
    res.json(files);
  } catch (e) {
    res.status(500).json({ error: e.toString() });
  }
});

interface Trip {
  id;
  user_id;
}
interface Point {
  id;
  trip_id;
  num;
  location;
  memo;
}
interface File {
  id;
  point_id;
}

let user_id = 1;

async function savePoints(points) {
  //   console.log('save trip:', points);
  console.log('save trip');
  let res = await client.query(
    `insert into trip (user_id) values ($1) returning id`,
    [user_id],
  );
  let trip_id = res.rows[0].id;
  let result = [];
  for (let point of points) {
    res = await client.query(
      `insert into point (trip_id, location, memo) values ($1,$2,$3) returning id`,
      [trip_id, point.location, point.memo],
    );
    let point_id = res.rows[0].id;
    result.push({ point_id, num: point.num });
  }
  return result;
}
async function saveFile(point_id, filename) {
  let res = await client.query(
    `insert into file (point_id, filename) values ($1,$2) returning id`,
    [point_id, filename],
  );
  return res.rows[0].id;
}

app.use(express.static('public'));
app.use('/uploads',express.static('uploads'))

app.listen(port, () => {
  console.log('listening on http://localhost:' + port);
});
